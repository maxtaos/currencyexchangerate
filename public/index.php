<?php

require_once (dirname(__DIR__) . '/vendor/autoload.php');

use Taos\Entities\Currency;
use Taos\Entities\ExchangeRateProvider;
use Taos\Entities\CachedExchangeRate;
use Taos\Entities\DBStoredExchangeRate;
use Taos\Entities\HTTPEchangeRate;

$currencyCode = "USD";

$currency = new Currency(
    $currencyCode,
    new ExchangeRateProvider([
        new CachedExchangeRate($currencyCode),
        new DBStoredExchangeRate($currencyCode),
        new HTTPEchangeRate($currencyCode)
    ])
);

echo "Значение курса для {$currencyCode}: " . $currency->rate() . "\n";