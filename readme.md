Для корректной работы автозагрузки необходимо выполнить `composer dumpautoload` перед запуском.

Для запуска выполните `php public/index.php`.

В методе **isExists()** в классах **CachedExchangeRate** и **DBStoredExchangeRate** для упрощения примера результат вычисляется случайным образом.

В методе **updateExchangeRate()** класса **ExchangeRateProvider** конструкция echo добавлена для наглядности процесса получения значения.