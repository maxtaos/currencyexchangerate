<?php

namespace Taos\Entities;

use Taos\Interfaces\IWritableExchangeRate;

class DBStoredExchangeRate extends ExchangeRate implements IWritableExchangeRate
{
    protected function getRateFromSource(): float
    {
        // реализация получения значения курса из базы
        return 65.35;
    }

    public function isExists(): bool
    {
        // проверяем есть ли в базе значение курса для $this->currencyCode
        return (bool)rand(0,1);
    }

    public function save(float $rate)
    {
        // сохраняем новое значение курса для $this->currencyCode
    }
}