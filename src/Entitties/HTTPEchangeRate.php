<?php

namespace Taos\Entities;

use Taos\Interfaces\IRemotedExchangeRate;

class HTTPEchangeRate extends ExchangeRate implements IRemotedExchangeRate
{
    protected function getRateFromSource(): float
    {
        // реализация получения значения курса через HTTP
        return 65.35;
    }
}