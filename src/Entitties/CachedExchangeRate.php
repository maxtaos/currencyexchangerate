<?php

namespace Taos\Entities;

use Taos\Interfaces\IWritableExchangeRate;

class CachedExchangeRate extends ExchangeRate implements IWritableExchangeRate
{
    protected function getRateFromSource(): float
    {
        // реализация получения значения курса из кэша
        return 65.35;
    }

    public function isExists(): bool
    {
        // проверяем есть ли в кэше значение курса для $this->currencyCode
        return (bool)rand(0,1);
    }

    public function save(float $rate)
    {
        // сохраняем в кэше новое значение курса для $this->currencyCode
    }
}