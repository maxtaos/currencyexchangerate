<?php

namespace Taos\Entities;

use Taos\Interfaces\ICurrency;

class Currency implements ICurrency
{
    private $currencyCode;
    private $exchangeRateProvider;

    public function __construct(string $currencyCode, ExchangeRateProvider $exchangeRateProvider)
    {
        $this->currencyCode = $currencyCode;
        $this->exchangeRateProvider = $exchangeRateProvider;
    }

    public function rate(): float
    {
        return $this->exchangeRateProvider->rate();
    }

    public function code(): string
    {
        return $this->currencyCode;
    }
}