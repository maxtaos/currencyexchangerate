<?php

namespace Taos\Entities;

use Taos\Interfaces\IExchangeRate;

abstract class ExchangeRate implements IExchangeRate
{
    private $currencyCode;

    public function __construct(string $currencyCode)
    {
        $this->currencyCode = $currencyCode;
    }

    public function rate(): float
    {
        return $this->getRateFromSource();
    }

    abstract protected function getRateFromSource(): float;
}