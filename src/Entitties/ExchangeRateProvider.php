<?php

namespace Taos\Entities;

use Taos\Interfaces\IExchangeRate;
use Taos\Interfaces\IRemotedExchangeRate;

class ExchangeRateProvider implements IExchangeRate
{
    private $exchangeRates;

    public function __construct(array $exchangeRates)
    {
        array_walk($exchangeRates, function(ExchangeRate $exchangeRate){
            $this->exchangeRates[] = $exchangeRate;
        });
    }

    public function rate(): float
    {
        if (!($this->exchangeRates[0] instanceof IRemotedExchangeRate)) {
            if (!$this->exchangeRates[0]->isExists()) {
                $this->updateExchangeRate(0);
            }
        }
        return $this->exchangeRates[0]->rate();
    }

    private function updateExchangeRate(int $exchangeRateIndex)
    {
        echo "Обновляем " . get_class($this->exchangeRates[$exchangeRateIndex]) . "\n";
        if (!$this->exchangeRates[$exchangeRateIndex] instanceof IRemotedExchangeRate) {
            if (!($this->exchangeRates[$exchangeRateIndex+1] instanceof IRemotedExchangeRate)) {
                if (!$this->exchangeRates[$exchangeRateIndex+1]->isExists()) {
                    $this->updateExchangeRate($exchangeRateIndex+1);
                }
            }
            $this->exchangeRates[$exchangeRateIndex]->save($this->exchangeRates[$exchangeRateIndex+1]->rate());
        }
    }
}