<?php

namespace Taos\Interfaces;

interface ICurrency
{
    public function rate(): float;

    public function code(): string;
}