<?php

namespace Taos\Interfaces;

interface IRemotedExchangeRate
{
    public function rate(): float;
}