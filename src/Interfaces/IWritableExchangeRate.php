<?php

namespace Taos\Interfaces;

interface IWritableExchangeRate
{
    public function isExists(): bool;

    public function save(float $rate);
}