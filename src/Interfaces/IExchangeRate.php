<?php

namespace Taos\Interfaces;

interface IExchangeRate
{
    public function rate(): float;
}